import requests as R
import uuid
from flask import Flask
from flask import render_template
from flask import request, jsonify
from flask import json
from flask import Response
from datetime import datetime
from datetime import timedelta
from os import path
import os;

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 60 * 1024 * 1024

UPLOAD_FOLDER = './uploaded'
ALLOWED_EXTENSIONS = set(['.mp3', '.m4a', '.wav'])
API_TOKEN = ""

@app.route('/')
def hello_world():
    return render_template('hello.html')

@app.route('/good-bye')
def bye_world():
    return render_template('good-bye.html')

@app.route("/podcast/validate", methods=['POST'])
def validate_form():
    errors = validate(request.json)

    if len(errors) > 0:
        return Response(json.dumps(errors), status=400, mimetype='application/json')

    return Response('{}', status=200, mimetype='application/json')


def validate(form_input):
    errors = {}

    if 'file_size' in form_input:
        if not form_input['file_extension'] in ALLOWED_EXTENSIONS:
            errors.update({'file' : "File extension should be one of the following: " + ', '.join(str(x) for x in ALLOWED_EXTENSIONS)})

        if form_input['file_size'] > app.config['MAX_CONTENT_LENGTH']:
            errors.update({'file' : "File size cannot exceed 50MB"})

        if form_input['file_size'] == -1:
            errors.update({'file' : "No audio file specified"})
    else:
        errors.update({'file_size' : "No audio file specified"})

    if 'name' in form_input:
        if len(form_input['name']) < 5:
            errors.update({'name' : "Name must be at lease 5 characters"})
    else:
        errors.update({'name' : "Please specify a name"})

    if 'title' in form_input:
        if len(form_input['title']) < 10:
            errors.update({'title' : "Title must be at lease 10 characters"})
        if len(form_input['title']) > 150:
            errors.update({'title' : "Title must not exceed 150 characters"})
    else:
        errors.update({'title' : "Please specify a name"})

    if 'summary' in form_input:
        if len(form_input['summary']) < 50:
            errors.update({'summary' : "Summary must be at lease 50 characters"})

        if len(form_input['summary']) > 200:
            errors.update({'summary' : "Summary must not exceed 200 characters"})
    else:
        errors.update({'summary' : "Please specify a summary"})

    if 'description' in form_input:
        if len(form_input['description']) < 50:
            errors.update({'description' : "Description must be at lease 50 characters"})

        if len(form_input['description']) > 500:
            errors.update({'description' : "Description must not exceed 500 characters"})
    else:
        errors.update({'description' : "Please specify a description"})

    return errors

@app.route('/podcast', methods=['POST'])
def create():

    file_extension = ""
    if len(request.files) > 0:
        if len(os.path.splitext(request.files['audio_file'].filename)) > 1:
            file_extension = os.path.splitext(request.files['audio_file'].filename)[1]

    data = {
        "title": request.form['title'],
        "description": request.form['description'],
        "summary": request.form['summary'],
        "name": request.form['name'],
        "file_size": len(request.files),
        "file_extension": file_extension,
        "artist": request.form['name'],
        "published_at": datetime.now() + timedelta(minutes=2)
    }
    

    errors = validate(data)
    if len(errors) > 0:
        return Response(json.dumps(errors), status=400,  mimetype='application/json')
       
    headers = { "Authorization" : "Token token=" + API_TOKEN }

    if not request.files:
        return "No file!"

    folder = uuid.uuid4().hex
    directory = UPLOAD_FOLDER + '/' + folder
    if not os.path.exists(directory):
        os.makedirs(directory)

    filename = request.files['audio_file'].filename;
    f = request.files['audio_file']
    f.save(directory + '/' + filename)
    # c = checksumMD5(f.stream)
    f.seek(0)

    with open(directory + '/' +'data.json', 'w') as outfile:
        json.dump(data, outfile)

    sendFile = {
        "audio_file": (f.filename, f.stream, f.mimetype)
    }

    r = R.post("https://www.buzzsprout.com/api/258619/episodes.json", headers=headers, data=data, files=sendFile)
    if r.status_code != 201:
        with open(directory + '/' +'failed.log', 'w') as outfile:
            json.dump(r.text, outfile)
            
    return render_template('good-bye.html')
