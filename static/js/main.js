const validate = async () => {

    let elements = ["name","description","title","summary","file"]
    elements.map((x) => {
        document.getElementById(x).className = "";
        document.getElementById("error-"+x).className = "";
    });

  let file_size = -1
  if(document.getElementById('file').files) {
    if(document.getElementById('file').files[0]) {
        file_size = document.getElementById('file').files[0].size
    }
  }

  fetch('./podcast/validate', {
    method: 'POST',
    body: JSON.stringify({
        "name": document.getElementById('name').value,
        "title": document.getElementById('title').value,
        "description": document.getElementById('description').value,
        "summary": document.getElementById('summary').value,
        "file": document.getElementById('file').value,
        "file_size": file_size,
        "file_extension": '.' + document.getElementById('file').value.split('.').pop(),

    }), // string or object
    headers:{
      'Content-Type': 'application/json'
    }
  })
  .then( (response) => response.json())
  .then( (data) => {
    // TODO - just use the status_code
    if (Object.keys(data).length > 0) {
        Toastify({
          text: "Opps! Could you please address the fields in red?",
          duration: 4000,
          gravity: "bottom", // `top` or `bottom`
          positionLeft: false, // `true` or `false`
          backgroundColor: "#00b09b",
        }).showToast();
    }
    return data;
  })
  .then( (data) => {
    Object.keys(data).map( (x) => {
        document.getElementById(x).className = "input-invalid";
        document.getElementById("error-"+x).className = "invalid";
        document.getElementById("error-"+x).innerHTML = data[x];
    })
    return data;
  })
  .then( (data) => {
    if (Object.keys(data).length != 0) return;
    
    // Animate the form disappering for submission
    const formWrapper =  document.querySelector('#form-wrapper')
    formWrapper.classList.add('animated', 'zoomOutLeft')
    formWrapper.addEventListener('animationend', () => {
        formWrapper.style.display = "none";
        
        // Display the indeterminate progress bar "uplading... " 
        const uploadingWrapper =  document.querySelector('#uploading-wrapper');
        uploadingWrapper.style.display = "inline";
        uploadingWrapper.classList.add('animated', 'zoomIn');
        
        // Acutally submit the form
        document.forms[0].submit();
    });
  });
}